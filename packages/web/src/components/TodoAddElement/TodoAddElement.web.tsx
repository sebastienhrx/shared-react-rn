import React from 'react'
import { TodoAddElementCore } from '../../../../shared/components/TodoAddElement.core';

// import './TodoAddElement.scss'

const TodoAddElementWeb = ({ inputValue, onInputChange, addValue }) => (
  <div>
    <input
      className='bg-gray-300 p-4 text-xl block w-full'
      type='text'
      placeholder='Nouvelle note'
      onChange={(e) => onInputChange(e.target.value)}
      value={inputValue} />
    <button
      className='m-auto block text-blue-800'
      type='button'
      onClick={addValue}
    >
      Add
    </button>
  </div>
);

export default TodoAddElementCore(TodoAddElementWeb);