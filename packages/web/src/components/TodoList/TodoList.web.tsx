import { TodoListCore } from '../../../../shared/components/TodoList.core';
import TodoAddElement from '../TodoAddElement/TodoAddElement.web';

const Item = ({ title, remove, index }) => {
  const onRemove = (index) => () => remove(index)
  return (
    <li
      onClick={onRemove(index)}
      className='bg-blue-200 text-white p-4 mb-1 text-xl'>
      <span className='text-xl'>{title}</span>
    </li>
  )
};

const TodoListWeb = ({ list, add, remove }) => (
  <div>
    <ul>
      {list.map((val, i) =>
        <Item
          remove={remove}
          index={i}
          key={i}
          title={val}
        />
      )}
    </ul>
    <TodoAddElement add={add} />
  </div>
);

export default TodoListCore(TodoListWeb);