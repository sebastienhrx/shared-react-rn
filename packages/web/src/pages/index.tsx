import TodoListWeb from '../components/TodoList/TodoList.web'

export default function Page() {
  return (
    <>
      <TodoListWeb />
    </>
  )
}
