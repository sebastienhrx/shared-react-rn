import { useState } from 'react'

export const TodoAddElementCore = (WrappedComponent) => (props) => {
  const [inputValue, setInputValue] = useState('');

  function onInputChange(text) {
    setInputValue(text);
  }

  function addValue() {
    props.add(inputValue);
    setInputValue('');
  }

  const passedProps = { ...props, inputValue, onInputChange, addValue };
  return <WrappedComponent {...passedProps} />;
};