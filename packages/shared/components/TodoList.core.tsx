import { useEffect, useState } from 'react'

const DATA = ['Pizza', 'Burger', 'Risotto'];

export const TodoListCore = (WrappedComponent) => (props) => {
  const [list, setList] = useState(DATA);
  const [sortList, setSortList] = useState([]);

  function add(val) {
    setList([...list, val]);
  }

  function remove(index) {
    setList(list.filter((el, i) => i !== index));
  }

  useEffect(() => {
    setSortList(list.sort((a, b) => a.localeCompare(b)));
  }, [list]);

  return <WrappedComponent {...props} list={sortList} add={add} remove={remove} />;
}
