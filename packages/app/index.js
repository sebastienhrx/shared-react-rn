import 'expo-dev-client'
import 'expo-dev-launcher'
import 'expo/build/Expo.fx'
import { activateKeepAwake } from 'expo-keep-awake'
import { registerRootComponent } from 'expo'

import App from './src/App.tsx';

if (__DEV__) {
  activateKeepAwake();
}

registerRootComponent(App);
