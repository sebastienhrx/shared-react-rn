import React from 'react'
import { enableScreens } from 'react-native-screens'
import { StatusBar } from 'expo-status-bar'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import TodoListApp from './components/TodoList.app'
import { SafeAreaView } from 'react-native'

enableScreens(true)

export default function App() {
  return (
      <SafeAreaProvider>
        <StatusBar style="dark" />
        <SafeAreaView>
          <TodoListApp />
        </SafeAreaView>
      </SafeAreaProvider>
  )
}
