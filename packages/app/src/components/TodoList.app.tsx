import React from 'react'
import tw from 'tailwind-react-native-classnames'
import { FlatList, Pressable, Text, View } from 'react-native'
import { TodoListCore } from '../../../shared/components/TodoList.core'
import TodoAddElement from './TodoAddElement.app'

const Item = ({ title, remove, index }) => (
  <Pressable
    onPress={() => remove(index)}
    style={tw`bg-blue-200 p-4 mb-1 `}
  >
    <Text style={tw`text-white text-xl`}>{title}</Text>
  </Pressable>
);

const TodoListApp = ({ list, add, remove }) => (
  <View>
    <FlatList
      data={list}
      keyExtractor={(item, index) => item + index}
      renderItem={({ item, index }) =>
        <Item title={item} remove={remove} index={index} />}
    />
    <TodoAddElement add={add} />
  </View>
);

export default TodoListCore(TodoListApp);