import React from 'react'
import { Button, TextInput, View } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import { TodoAddElementCore } from '../../../shared/components/TodoAddElement.core';

const TodoAddElementApp = ({ inputValue, onInputChange, addValue }) => (
  <View>
    <TextInput
      style={tw`bg-gray-300 p-4 text-xl`}
      placeholder='Nouvelle note'
      onChangeText={text => onInputChange(text)}
      value={inputValue} />
    <Button onPress={addValue} title='Add' color={tw.color('blue-800')} />
  </View>
);

export default TodoAddElementCore(TodoAddElementApp);