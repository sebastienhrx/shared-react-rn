const colors = require('tailwindcss/colors');

module.exports = {
  mode: 'jit',
  purge: ['./packages/web/pages/**/*.{js,ts,jsx,tsx}', './packages/web/components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    colors: {
      primary: {
        400: '#55c2ae',
        700: '#287265'
      },
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      green: colors.green,
      indigo: colors.indigo,
      blue: colors.blue,
      red: colors.rose,
      yellow: colors.amber
    },
    extend: {}
  },
  variants: {
    extend: {}
  },
  plugins: []
};
